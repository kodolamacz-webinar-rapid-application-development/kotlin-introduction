package pl.kodolamacz.webinar.introduction

fun main() {

    // isTrue ? x : y

    val x: String = if (false) "Mark" else "Bob"
    println(x)

    when (6) {
        1 -> println("one")
        in 5..10 -> println("five to ten")
        else -> println("else")
    }

    when (Directions.NORTH) {
        Directions.NORTH -> println("go north")
        Directions.EAST -> println("go east")
        Directions.WEST -> println("go west")
        Directions.SOUTH -> println("go south")
    }
}

enum class Directions {
    NORTH, EAST, WEST, SOUTH
}
