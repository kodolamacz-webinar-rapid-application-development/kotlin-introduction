package pl.kodolamacz.webinar.introduction

fun main() {
    val person = Person("Mark", "Twain")
    val person2 = Person("Mark")
    println(person == person2)
    println(person)

    val person3: Named = person.copy(name = "Bob")
    println(person3)


    when (person3) {
        is Dog -> println("is dog")
        is Person -> println("is person")
    }
}

// toString, equals, hashCode, copy, componentN
data class Person(val name: String, var surname: String? = null) : Named
data class Dog(val age: Int) : Named

sealed interface Named

open class Base
class Derived : Base()
