package pl.kodolamacz.webinar.introduction

fun main() {

    // String name = "Mark"
    var name = "Mark"
    println(name)
    name = "Bob"
    println(name)

    val age = 60
    println(age)

    val surname = "Twain"

    val fullName = "$name $surname"
    println(fullName)

    val length = "length: ${name.length}"
    println(length)

    val test = "a\nb"
    println(test)

    val row = """a
        |b""".trimMargin()
    println(row)


    val integer = 5
    val long = 5L
    val sum = integer + long


    val short: Short = 2
    val byte: Byte = 2
    val sum2 = short + byte
}
