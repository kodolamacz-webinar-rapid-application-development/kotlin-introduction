package pl.kodolamacz.webinar.introduction

fun main() {

    // (String) obj
    // if(x instanceof String)
    val x: Any = 5

    if (x is String) {
        println(x)
    }

    // Consumer -> in
    // Producer -> out
    val integers: List<Int> = listOf(1, 2)
    println(integers)

    val numbers: List<Number> = integers
    println(numbers)
}
