package pl.kodolamacz.webinar.introduction

fun main() {

    val name: String? = null
    val age: Int? = null

    println(name?.length)

//    throws NPE
//    println(name!!.length)
}
