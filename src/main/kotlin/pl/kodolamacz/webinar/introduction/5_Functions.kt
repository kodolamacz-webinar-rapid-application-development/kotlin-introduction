package pl.kodolamacz.webinar.introduction

fun main() {
    greet(name = "Mark", greeting = "Hello")
    greet(name = "Bob")
    greet(greeting = "Hi")
    greet()

    println(getGreetingToPrint("hello", "Mark"))

    val names: Array<String> = arrayOf("Mark", "Bob")
    greetAll("Hello", names = names)

    val numbers: MutableList<Int> = mutableListOf(1, 2, 3, 4)
    println(numbers)
    println(numbers.swap(0, 3))

    val number = 5
    println(number sum 5)
}

fun greet(greeting: String = "Hello", name: String = "World") = println("$greeting $name")

fun getGreetingToPrint(greeting: String, name: String) = "$greeting $name"

fun greetAll(greeting: String = "Hello", vararg names: String) =
    names.forEach { println("$greeting $it") }

fun MutableList<Int>.swap(index1: Int, index2: Int): MutableList<Int> {
    val temp = this[index1]
    this[index1] = this[index2]
    this[index2] = temp
    return this
}

infix fun Int.sum(a: Int) = this + a
